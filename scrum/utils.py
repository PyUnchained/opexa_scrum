def round_float(num, up = False):
	round_value = 0
	if up:
		round_value = 0.5
	return int((num*100)+round_value)/100.00

def return_pk_list(obj_list):
	prexisting_pks = []
	for item in obj_list:
	    prexisting_pks.append(item.pk)
	return prexisting_pks
