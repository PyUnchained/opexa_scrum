# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('scrum', '0003_auto_20160812_1620'),
    ]

    operations = [
        migrations.AddField(
            model_name='sprint',
            name='complete',
            field=models.BooleanField(default=False),
        ),
    ]
