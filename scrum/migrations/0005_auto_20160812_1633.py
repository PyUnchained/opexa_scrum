# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('scrum', '0004_sprint_complete'),
    ]

    operations = [
        migrations.AddField(
            model_name='backlogentry',
            name='short_description',
            field=models.CharField(max_length=100, null=True),
        ),
        migrations.AlterField(
            model_name='backlogentry',
            name='description',
            field=models.TextField(max_length=300, null=True, blank=True),
        ),
    ]
