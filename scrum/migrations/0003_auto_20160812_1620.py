# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models
import scrum.models


class Migration(migrations.Migration):

    dependencies = [
        ('scrum', '0002_backlogentry_project'),
    ]

    operations = [
        migrations.AlterField(
            model_name='sprint',
            name='review_summary',
            field=models.FileField(null=True, upload_to=scrum.models.review_summary_upload_to, blank=True),
        ),
    ]
