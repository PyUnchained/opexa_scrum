# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('scrum', '0007_auto_20160812_1808'),
    ]

    operations = [
        migrations.AddField(
            model_name='sprint',
            name='name',
            field=models.CharField(max_length=100, null=True, blank=True),
        ),
        migrations.AlterField(
            model_name='sprint',
            name='goal',
            field=models.TextField(max_length=200),
        ),
    ]
