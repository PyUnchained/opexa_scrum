# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models
import scrum.models
from django.conf import settings


class Migration(migrations.Migration):

    dependencies = [
        migrations.swappable_dependency(settings.AUTH_USER_MODEL),
    ]

    operations = [
        migrations.CreateModel(
            name='BacklogCategory',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('name', models.CharField(max_length=100)),
            ],
        ),
        migrations.CreateModel(
            name='BacklogEntry',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('description', models.TextField(max_length=300)),
                ('priority', models.IntegerField(default=5, help_text=b'Select a value between 0 (low) and 10 (high)')),
                ('complete', models.BooleanField(default=False)),
                ('category', models.ForeignKey(to='scrum.BacklogCategory')),
            ],
        ),
        migrations.CreateModel(
            name='Client',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('name', models.CharField(max_length=100)),
            ],
        ),
        migrations.CreateModel(
            name='Person',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('cell', models.CharField(max_length=30, null=True, blank=True)),
                ('skype', models.CharField(max_length=50, null=True, blank=True)),
                ('user', models.ForeignKey(to=settings.AUTH_USER_MODEL)),
            ],
        ),
        migrations.CreateModel(
            name='Project',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('name', models.CharField(max_length=50)),
                ('administrator', models.ForeignKey(blank=True, to='scrum.Person', null=True)),
                ('client', models.ForeignKey(to='scrum.Client')),
                ('contributors', models.ManyToManyField(related_name='con_persons', to='scrum.Person')),
            ],
        ),
        migrations.CreateModel(
            name='Sprint',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('goal', models.TextField(max_length=160)),
                ('start', models.DateField()),
                ('end', models.DateField()),
                ('review_summary', models.FileField(upload_to=scrum.models.review_summary_upload_to)),
                ('contributors', models.ManyToManyField(to='scrum.Person')),
                ('items', models.ManyToManyField(to='scrum.BacklogEntry')),
                ('project', models.ForeignKey(to='scrum.Project')),
            ],
        ),
        migrations.AddField(
            model_name='client',
            name='contact_persons',
            field=models.ManyToManyField(to='scrum.Person'),
        ),
        migrations.AddField(
            model_name='backlogcategory',
            name='project',
            field=models.ForeignKey(to='scrum.Project'),
        ),
    ]
