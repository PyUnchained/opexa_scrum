# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('scrum', '0006_auto_20160812_1639'),
    ]

    operations = [
        migrations.CreateModel(
            name='SprintReview',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('worked', models.TextField(max_length=500, null=True, verbose_name=b'What worked well?', blank=True)),
                ('improvements', models.TextField(max_length=500, null=True, verbose_name=b'What worked well?', blank=True)),
                ('rating', models.IntegerField(help_text=b'Select a value between 0 (low) and 10 (high)')),
                ('external_review', models.BooleanField(default=False)),
                ('author', models.ForeignKey(to='scrum.Person')),
            ],
        ),
        migrations.AlterField(
            model_name='sprint',
            name='contributors',
            field=models.ManyToManyField(to='scrum.Person', blank=True),
        ),
        migrations.AddField(
            model_name='sprintreview',
            name='sprint',
            field=models.ForeignKey(to='scrum.Sprint'),
        ),
    ]
