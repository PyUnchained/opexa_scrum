# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('scrum', '0005_auto_20160812_1633'),
    ]

    operations = [
        migrations.AlterField(
            model_name='sprint',
            name='goal',
            field=models.CharField(max_length=160),
        ),
        migrations.AlterField(
            model_name='sprint',
            name='items',
            field=models.ManyToManyField(to='scrum.BacklogEntry', verbose_name=b'Backlog Covered'),
        ),
    ]
