from django import forms
from django.db.models import Q

from models import BacklogEntry, Project, Sprint
from utils import return_pk_list


class SprintAdminForm(forms.ModelForm):
    """
    Custom admin form to only display Backlog Entries
    related to this sprint.
    """
    def __init__(self, *args, **kwargs):
        super(SprintAdminForm, self).__init__(*args, **kwargs)
        # print self.instance.project
        try:
            prexisting_pks = return_pk_list(self.instance.items.all())
            self.fields['items'].queryset = BacklogEntry.objects.filter(
                Q(project=self.instance.project) & Q(complete = False) | Q(pk__in = prexisting_pks))
        except Project.DoesNotExist:
            self.fields['items'].queryset = BacklogEntry.objects.filter(
                Q(description='self.instance.project'))
