from __future__ import division

from time import strftime

from django.db import models
from django.contrib.auth.models import User
from django.core.validators import MinValueValidator, MaxValueValidator

from utils import round_float
# Create your models here.
class Person(models.Model):
    user = models.ForeignKey(User)
    cell = models.CharField(max_length = 30,
        blank=True, null = True)
    skype = models.CharField(max_length = 50,
        blank=True, null = True)

    def __str__(self):
        return self.user.username

class Client(models.Model):
    name = models.CharField(max_length = 100)
    contact_persons = models.ManyToManyField('Person')

    def __str__(self):
        return self.name

class Project(models.Model):

    name = models.CharField(max_length = 50)
    contributors = models.ManyToManyField('Person',
        related_name = 'con_persons', blank = True)
    client = models.ForeignKey('Client')
    administrator = models.ForeignKey('Person',
        blank = True, null = True)

    def __str__(self):
        return self.name

    @property
    def sprint_completion(self):
        percentage = 0.00
        backlog = Sprint.objects.filter(
            project = self)

        total = len(backlog)
        complete = 0
        if total != 0:
            for sprint in backlog:
                if sprint.percentage_complete >= 100:
                    complete += 1

        return '{0}/{1}'.format(complete, total)



class BacklogCategory(models.Model):
    project = models.ForeignKey('Project')
    name = models.CharField(max_length = 100)

    def __str__(self):
        return self.name

class BacklogEntry(models.Model):
    project = models.ForeignKey('Project', null = True)
    category = models.ForeignKey('BacklogCategory')
    short_description = models.CharField(max_length = 100,
        null = True)
    description = models.TextField(max_length = 300,
        blank = True, null = True)
    priority = models.IntegerField(default = 5,
        validators = [MinValueValidator(0),
            MaxValueValidator(10)],
        help_text = 'Select a value between 0 (low) and 10 (high)')
    complete = models.BooleanField(default = False)

    def __str__(self):
        return str(self.short_description)

def review_summary_upload_to(self, filename):
    url = "review_summary/%s/%s" % (
        self.project.name,
        filename)
    return url

class Sprint(models.Model):
    project = models.ForeignKey('Project')
    name = models.CharField(max_length = 100,
        null = True)
    goal = models.TextField(max_length = 200)
    start = models.DateField()
    end = models.DateField()
    contributors = models.ManyToManyField('Person',
        blank = True)
    items = models.ManyToManyField('BacklogEntry',
        verbose_name = 'Backlog Covered')
    review_summary = models.FileField(
        upload_to = review_summary_upload_to,
        blank = True, null = True)
    complete = models.BooleanField(default = False)

    def __str__(self):
        return_str = self.project.name + ' '
        if self.name:
            return_str += self.name
        else:
            return_str += str(self.start)
        return  return_str

    @property
    def percentage_complete(self):
        save = False
        percentage = 0.00
        backlog = self.items.all()
        total = len(backlog)

        #Calculate the percentage of entries
        #that are complete
        if total != 0:
            complete = 0
            for entry in backlog:
                if entry.complete:
                    complete += 1

            percentage = (complete/total)*100

        
        #Set whether or not the sprint can be considered
        #complete
        if self.complete:
            if percentage < 100:
                self.complete = False
                save = True
        else:
            if percentage >= 100:
                self.complete = True
                save = True

        if save:
            self.save()

        return round_float(percentage)

    @property
    def external_review_score(self):
        tot = 0
        all_reviews = SprintReview.objects.filter(sprint = self,
            external_review = True)
        if len(all_reviews) == 0:
            return 'N/A'

        for review in all_reviews:
            if review.rating:
                tot += review.rating

        average = tot/len(all_reviews)


        return round_float(average/10*100)

    @property
    def team_review_score(self):
        tot = 0
        all_reviews = SprintReview.objects.filter(sprint = self)
        if len(all_reviews) == 0:
            return 'N/A'

        for review in all_reviews:
            if review.rating:
                tot += review.rating

        average = tot/len(all_reviews)


        return round_float(average/10*100)

class SprintReview(models.Model):
    sprint = models.ForeignKey('Sprint')
    author = models.ForeignKey('Person')
    worked = models.TextField(max_length = 500,
        blank = True, null = True,
        verbose_name = 'What worked well?')
    improvements = models.TextField(max_length = 500,
        blank = True, null = True,
        verbose_name = 'Possible improvements?')
    rating = models.IntegerField(
        validators = [MinValueValidator(0),
            MaxValueValidator(10)],
        help_text = 'Select a value between 0 (low) and 10 (high)')
    external_review = models.BooleanField(
        default = False)