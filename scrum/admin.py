from django.contrib import admin


from models import (BacklogCategory, BacklogEntry,
    Client, Person, Project, Sprint, SprintReview)
from forms import SprintAdminForm


class BacklogEntryInline(admin.TabularInline):
    model = BacklogEntry
    verbose_name = 'Backlog Entry'
    verbose_name_plural = 'Backlog Entries'
    extra = 0
    ordering = ['-priority']

class SprintInline(admin.StackedInline):
    model = Sprint 
    form = SprintAdminForm
    extra = 0
    ordering = ['start']
    fields = [('goal','name','start', 'end'), 'items',
        ('review_summary', 'complete')]
    filter_horizontal = ['items']

class ReviewInline(admin.TabularInline):
    model = SprintReview
    extra = 0

class HiddenAdmin(admin.ModelAdmin):
    def get_model_perms(self, request):
        """
        Return empty perms dict thus hiding the model from admin index.
        """
        return {}

class SprintAdmin(admin.ModelAdmin):
    form = SprintAdminForm
    inlines = [ReviewInline]
    list_display = ['name', 'project', 'start', 'end', 'complete', 'percentage_complete',
        'team_review_score', 'external_review_score', 'review_summary']
    filter_horizontal = ['contributors', 'items']
    normal_fields = [('project', 'start', 'end'),
        ('name','goal'), 'items', 'contributors',
        ('review_summary', 'complete')]

    def external_review_score(self, obj):
        return obj.external_review_score

    def team_review_score(self, obj):
        return obj.team_review_score

    def percentage_complete(self, obj):
        return obj.percentage_complete

    def get_form(self, request, obj=None, **kwargs):
        """
        Customize form depending on whether or not it refers to an object
        that already exists on the system.
        """

        if obj:
            self.fields = self.normal_fields
        else:
            self.fields = [('project', 'start', 'end'),
                ('name','goal')]

        return super(SprintAdmin, self).get_form(request, obj, **kwargs)


class ProjectAdmin(admin.ModelAdmin):
    filter_horizontal = ['contributors']
    list_display = ['name', 'client',
    'sprint_completion']
    inlines = [SprintInline,BacklogEntryInline]

    def sprint_completion(self, obj):
        return obj.sprint_completion

    def project_contributors(self, obj):

        return obj.project_contributors

class ClientAdmin(admin.ModelAdmin):
    filter_horizontal = ['contact_persons']

admin.site.register(BacklogCategory, HiddenAdmin)
admin.site.register(BacklogEntry, HiddenAdmin)
admin.site.register(Client, ClientAdmin)
admin.site.register(Person)
admin.site.register(SprintReview, HiddenAdmin)
admin.site.register(Project, ProjectAdmin)
admin.site.register(Sprint, SprintAdmin)